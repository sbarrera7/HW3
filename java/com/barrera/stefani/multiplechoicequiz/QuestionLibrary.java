package com.barrera.stefani.multiplechoicequiz;

//Questions for quiz
class QuestionLibrary {
    private String mQuestions[] = {
            "Which is heavier a ton of gold or a ton of silver?",
            "Is it legal for a man to marry his widows sister?",
            "Why is it against the law for a person living in New York to be buried in California?",
            "Which month has 28 days?",
            "How many times can you subtract 10 from 100?",
            "Adams mother had three children. The first child was named April. The second child was named May. What was the third childs name?",
            "A clerk at a butcher shop stands five-feet ten-inches tall and wears size 13 sneakers.What does he weigh?",
            "Before Mt. Everest was discovered, which was the highest Mountain in the world?",
            "If you were running a race and you passed the person in second place, what place would you be in now?",
            "If an electric train is traveling South, which way is the smoke going?"

    };

    //Choices given
    private String mChoices [][] = {
            {"Gold", "Silver", "Equal"},
            {"Yes", "In some states", "No"},
            {"He is not dead", "He needs to be buried where he died", "That's just the way the law is"},
            {"All", "First 6 months of the year", "February"},
            {"Ten", "Five", "Once"},
            {"Adam","March", "June"},
            {"200 lbs", "160 lbs", "None of the above"},
            {"Mt. Aconcagua", "Mt. Everest", "Mt. Mckinley"},
            {"1st place", "2nd place", "3rd place"},
            {"North", "West", "Neither"}

    };

    //Correct answers
    private String mCorrectAnswers [] = {"Equal", "No", "He is not dead", "All", "Once", "Adam", "None of the above", "Mt. Everest", "2nd place", "Neither"};


    public String getQuestion(int a){
        String question;
        question = mQuestions[a];
        return question;
    }

    public String getChoice1(int a) {
        String choice0= mChoices[a][0];
        return choice0;
    }

    public String getChoice2 (int a) {
        String choice1= mChoices[a][1];
        return choice1;
    }

    public String getChoice3 (int a) {
        String choice2= mChoices[a][2];
        return choice2;
    }

    String getCorrectAnswer(int a){
        return mCorrectAnswers[a][3];
    }





}
